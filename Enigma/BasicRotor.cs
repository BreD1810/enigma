using System.Collections.Generic;

namespace BreD.Enigma
{
    public class BasicRotor : Rotor
    {
        
        private readonly Dictionary<RotorType, int[]> _mappings = new Dictionary<RotorType, int[]>()
        {
            {RotorType.I, new[] {4, 10, 12, 5, 11, 6, 3, 16, 21, 25, 13, 19, 14, 22, 24, 7, 23, 20, 18, 15, 0, 8, 1, 17, 2, 9}},
            {RotorType.II, new[] {0, 9, 3, 10, 18, 8, 17, 20, 23, 1, 11, 7, 22, 19, 12, 2, 16, 6, 25, 13, 15, 24, 5, 21, 14, 4}},
            {RotorType.III, new[] {1, 3, 5, 7, 9, 11, 2, 15, 17, 19, 23, 21, 25, 13, 24, 4, 8, 22, 6, 0, 10, 12, 20, 18, 16, 14}},
            {RotorType.IV, new[] {4, 18, 14, 21, 15, 25, 9, 0, 24, 16, 20, 8, 17, 7, 23, 11, 13, 5, 19, 6, 10, 3, 2, 12, 22, 1}},
            {RotorType.V, new[] { 21, 25, 1, 17, 6, 8, 19, 24, 20, 15, 18, 3, 13, 7, 11, 23, 0, 22, 12, 9, 16, 14, 5, 4, 2, 10}}
        };
        private int[] InverseMapping { get; }

        public BasicRotor(RotorType type)
        {
            Type = type;
            Mapping = _mappings[Type];
            
            var inverseMapping = new int[Mapping.Length];
            for (var i = 0; i < Mapping.Length; i++)
            {
                inverseMapping[Mapping[i]] = i;
            }
            InverseMapping = inverseMapping;
        }
        
        public override int Substitute(int input)
        {
            input -= Position;
            if (input < 0)
            {
                input += RotorSize;
            }
        
            var character = Mapping[input];
        
            character += Position;
            if (character > RotorSize - 1)
            {
                character -= RotorSize;
            }
        
            return character;
        }
        
        public int SubstituteBack(int input)
        {
            input -= Position;
            if (input < 0)
            {
                input += RotorSize;
            }
        
            var character = InverseMapping[input];
        
            character += Position;
            if (character > RotorSize - 1)
            {
                character -= RotorSize;
            }
        
            return character;
        }

        public virtual void Rotate() => Position = (Position + 1) % RotorSize;
    }
}