using System.Collections.Generic;
using System.Linq;

namespace BreD.Enigma
{
    public class Plugboard
    {

        private List<Plug> _plugs = new List<Plug>(13);
        
        public bool AddPlug(char end1, char end2)
        {
            if (_plugs.Count >= 13)
            {
                return false;
            }
            var newPlug = new Plug(end1, end2);
            if (_plugs.Any(plug => newPlug.ClashesWith(plug))) return false;
            _plugs.Add(newPlug);
            return true;
        }

        public int GetNumPlugs() => _plugs.Count;

        public void Clear() => _plugs.Clear();

        public char Substitute(char c)
        {
            foreach (var plug in _plugs)
            {
                if (plug.End1 == c)
                {
                    return plug.End2;
                }
                else if (plug.End2 == c)
                {
                    return plug.End1;
                }
            }

            return c;
        }
        
    }
}