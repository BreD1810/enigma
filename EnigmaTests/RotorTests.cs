using Xunit;

namespace BreD.Enigma.Tests
{
    public class RotorTests
    {

        [Fact]
        public void ReflectorSubstitute()
        {
            var reflector = new Reflector(RotorType.ReflectorI);
            Assert.Equal(24, reflector.Substitute(0));
            var reflector2 = new Reflector(RotorType.ReflectorII);
            Assert.Equal(5, reflector2.Substitute(0));
        }

        [Fact]
        public void BasicRotorSubstitute()
        {
            var rotor = new BasicRotor(RotorType.I);
            Assert.Equal(4, rotor.Substitute(0));
            
            var rotor2 = new BasicRotor(RotorType.II);
            Assert.Equal(0, rotor2.Substitute(0));
            
            var rotor3 = new BasicRotor(RotorType.III);
            Assert.Equal(1, rotor3.Substitute(0));
            
            var rotor4 = new BasicRotor(RotorType.IV);
            Assert.Equal(4, rotor4.Substitute(0));
            
            var rotor5 = new BasicRotor(RotorType.V);
            Assert.Equal(21, rotor5.Substitute(0));
        }

        [Fact]
        public void BasicRotorSubstituteBack()
        {
            var rotor = new BasicRotor(RotorType.I);
            Assert.Equal(0, rotor.SubstituteBack(4));
            
            var rotor2 = new BasicRotor(RotorType.II);
            Assert.Equal(0, rotor2.SubstituteBack(0));
            
            var rotor3 = new BasicRotor(RotorType.III);
            Assert.Equal(0, rotor3.SubstituteBack(1));
            
            var rotor4 = new BasicRotor(RotorType.IV);
            Assert.Equal(0, rotor4.SubstituteBack(4));
            
            var rotor5 = new BasicRotor(RotorType.V);
            Assert.Equal(0, rotor5.SubstituteBack(21));
        }

        [Fact]
        public void BasicRotorPosition()
        {
            var rotor = new BasicRotor(RotorType.I);
            Assert.Equal(0, rotor.Position);
            rotor.Rotate();
            Assert.Equal(1, rotor.Position);
            for (var i = 0; i < 24; i++)
            {
                rotor.Rotate();
            }
            Assert.Equal(25, rotor.Position);
            rotor.Rotate();
            Assert.Equal(0, rotor.Position);
        }
        
    }
}