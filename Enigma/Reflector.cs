using System.Collections.Generic;

namespace BreD.Enigma
{
    public class Reflector : Rotor
    {
        
        private readonly Dictionary<RotorType, int[]> _mappings = new Dictionary<RotorType, int[]>()
        {
            {RotorType.ReflectorI, new[] {24, 17, 20, 7, 16, 18, 11, 3, 15, 23, 13, 6, 14, 10, 12, 8, 4, 1, 5, 25, 2, 22, 21, 9, 0, 19 }},
            {RotorType.ReflectorII, new[] { 5, 21, 15, 9, 8, 0, 14, 24, 4, 3, 17, 25, 23, 22, 6, 2, 19, 10, 20, 16, 18, 1, 13, 12, 7, 11 }}
        };

        public Reflector(RotorType type)
        {
            Type = type;
            Mapping = _mappings[Type];
        }
        
        public override int Substitute(int input) => Mapping[input];
    }
}