namespace BreD.Enigma
{
    public enum RotorType
    {
        I,
        II,
        III,
        IV,
        V,
        ReflectorI,
        ReflectorII
    }
}