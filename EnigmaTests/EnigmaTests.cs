using System.Text;
using Xunit;

namespace BreD.Enigma.Tests
{
    public class EnigmaTests
    {
        
        [Fact]
        public void Test1()
        {
            var enigmaMachine = new EnigmaMachine();
            
            enigmaMachine.AddPlug('A', 'M');
            enigmaMachine.AddPlug('G', 'L');
            enigmaMachine.AddPlug('E', 'T');

            enigmaMachine.AddRotor(new BasicRotor(RotorType.I), 0);
            enigmaMachine.AddRotor(new BasicRotor(RotorType.II), 1);
            enigmaMachine.AddRotor(new BasicRotor(RotorType.III), 2);

            enigmaMachine.SetPosition(0, 6);
            enigmaMachine.SetPosition(1, 12);
            enigmaMachine.SetPosition(2, 5);
            
            enigmaMachine.Reflector = new Reflector(RotorType.ReflectorI);
            const string encodedMessage = "GFWIQH";
            var messageBuilder = new StringBuilder();
            foreach (var c in encodedMessage)
            {
                messageBuilder.Append(enigmaMachine.EncodeLetter(c));
            }
            
            Assert.Equal("BADGER", messageBuilder.ToString());
        }
        
        [Fact]
        public void Test2()
        {
            var enigmaMachine = new EnigmaMachine();
            
            enigmaMachine.AddPlug('B', 'C');
            enigmaMachine.AddPlug('R', 'I');
            enigmaMachine.AddPlug('S', 'M');
            enigmaMachine.AddPlug('A', 'F');

            enigmaMachine.AddRotor(new BasicRotor(RotorType.IV), 0);
            enigmaMachine.AddRotor(new BasicRotor(RotorType.V), 1);
            enigmaMachine.AddRotor(new BasicRotor(RotorType.II), 2);

            enigmaMachine.SetPosition(0, 23);
            enigmaMachine.SetPosition(1, 4);
            enigmaMachine.SetPosition(2, 9);
            
            enigmaMachine.Reflector = new Reflector(RotorType.ReflectorII);
            const string encodedMessage = "GACIG";
            var messageBuilder = new StringBuilder();
            foreach (var c in encodedMessage)
            {
                messageBuilder.Append(enigmaMachine.EncodeLetter(c));
            }
            
            Assert.Equal("SNAKE", messageBuilder.ToString());
        }

        [Fact]
        public void Test3()
        {
            var enigmaMachine = new EnigmaMachine();
            
            enigmaMachine.AddPlug('Q', 'F');
            
            enigmaMachine.AddRotor(new TurnoverRotor(RotorType.I), 0);
            enigmaMachine.AddRotor(new TurnoverRotor(RotorType.II), 1);
            enigmaMachine.AddRotor(new TurnoverRotor(RotorType.III), 2);
            
            enigmaMachine.SetPosition(0, 23);
            enigmaMachine.SetPosition(1, 11);
            enigmaMachine.SetPosition(2, 7);
            
            enigmaMachine.Reflector = new Reflector(RotorType.ReflectorI);
            
            const string encodedMessage = "OJVAYFGUOFIVOTAYRNIWJYQWMXUEJGXYGIFT";
            var messageBuilder = new StringBuilder();
            foreach (var c in encodedMessage)
                messageBuilder.Append(enigmaMachine.EncodeLetter(c));
            Assert.Equal("THEQUICKBROWNFOXJUMPEDOVERTHELAZYDOG", messageBuilder.ToString());
        }
        
    }
}
