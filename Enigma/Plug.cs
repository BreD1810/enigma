namespace BreD.Enigma
{
    public class Plug
    {

        public char End1 { get; }
        public char End2 { get; }

        public Plug(char end1, char end2)
        {
            End1 = end1;
            End2 = end2;
        }


        public char Encode(char letterIn)
        {
            if (End1 == letterIn)
            {
                return End2;
            }
            else if (End2 == letterIn)
            {
                return End1;
            }
            return letterIn;
        }

        public bool ClashesWith(Plug plugIn)
        {
            return plugIn.End1 == End1 || plugIn.End1 == End2 || plugIn.End2 == End1 || plugIn.End2 == End2;
        }
        
    }
}