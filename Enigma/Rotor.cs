namespace BreD.Enigma
{
    public abstract class Rotor
    {
        protected RotorType Type;
        public int Position { get; set; }
        protected int[] Mapping = new int[RotorSize];
        public const int RotorSize = 26;

        public abstract int Substitute(int input);

    }
}