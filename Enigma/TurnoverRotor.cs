using System;
using System.Collections.Generic;

namespace BreD.Enigma
{
    public class TurnoverRotor : BasicRotor
    {
        
        private static readonly Dictionary<RotorType, int> TurnoverPositions = new Dictionary<RotorType, int>()
        {
            {RotorType.I, 24},
            {RotorType.II, 12},
            {RotorType.III, 3},
            {RotorType.IV, 17},
            {RotorType.V, 7}
        };

        public event Action IsTurningOver;

        public TurnoverRotor(RotorType type) : base(type) { }

        public override void Rotate()
        {
            base.Rotate();
            if (Position == TurnoverPositions[Type])
                IsTurningOver?.Invoke();
        }

    }
}