using Xunit;

namespace BreD.Enigma.Tests
{
    public class PlugTests
    {

        [Fact]
        public void PlugClashes()
        {
            var plug1 = new Plug('a', 'b');
            var plug2 = new Plug('b', 'c');
            Assert.True(plug1.ClashesWith(plug2));
        }

        [Fact]
        public void PlugDoesntClash()
        {
            var plug1 = new Plug('a', 'b');
            var plug2 = new Plug('x', 'y');
            Assert.False(plug1.ClashesWith(plug2));
        }

        [Fact]
        public void Encoding()
        {
            var plug = new Plug('a', 'b');
            Assert.Equal('b', plug.Encode('a'));
            Assert.Equal('x', plug.Encode('x'));
        }
        
    }
}