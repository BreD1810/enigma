using Xunit;

namespace BreD.Enigma.Tests
{
    public class PlugboardTests
    {

        [Fact]
        public void AddingPlug()
        {
            var plugboard = new Plugboard();
            Assert.True(plugboard.AddPlug('a', 'b'));
            Assert.Equal(1, plugboard.GetNumPlugs());
            Assert.True(plugboard.AddPlug('c', 'd'));
            Assert.Equal(2, plugboard.GetNumPlugs());
        }

        [Fact]
        public void DuplicatePlug()
        {
            var plugboard = new Plugboard();
            Assert.True(plugboard.AddPlug('a', 'b'));
            
            Assert.False(plugboard.AddPlug('b', 'c'));
            Assert.Equal(1, plugboard.GetNumPlugs());
            
            Assert.False(plugboard.AddPlug('c', 'a'));
            Assert.Equal(1, plugboard.GetNumPlugs());
            
            Assert.True(plugboard.AddPlug('c', 'd'));
            Assert.Equal(2, plugboard.GetNumPlugs());
        }

        [Fact]
        public void Clearing()
        {
            var plugboard = new Plugboard();
            plugboard.AddPlug('a', 'b');
            Assert.Equal(1, plugboard.GetNumPlugs());
            plugboard.Clear();
            Assert.Equal(0, plugboard.GetNumPlugs());
            Assert.True(plugboard.AddPlug('a', 'b'));
        }

        [Fact]
        public void Substitution()
        {
            var plugboard = new Plugboard();
            plugboard.AddPlug('a', 'b');
            plugboard.AddPlug('x', 'y');
            Assert.Equal('b', plugboard.Substitute('a'));
            Assert.Equal('a', plugboard.Substitute('b'));
            Assert.Equal('c', plugboard.Substitute('c'));
            Assert.Equal('y', plugboard.Substitute('x'));
            Assert.Equal('x', plugboard.Substitute('y'));
        }
        
    }
}