using System.Collections.Generic;
using System.Linq;

namespace BreD.Enigma
{
    public class EnigmaMachine
    {

        private Plugboard _plugboard;
        private BasicRotor[] _rotors = new BasicRotor[3];
        public Reflector Reflector { get; set; }

        public EnigmaMachine()
        {
            _plugboard = new Plugboard();
        }

        public void ClearPlugboard() => _plugboard.Clear();

        public void AddPlug(char a, char b) => _plugboard.AddPlug(a, b);

        public void AddRotor(BasicRotor rotor, int slot)
        {
            _rotors[slot] = rotor;
            if (rotor is TurnoverRotor t && slot < _rotors.Length - 1)
                t.IsTurningOver += () => _rotors[slot + 1].Rotate();
        }

        public BasicRotor GetRotor(int slot) => _rotors[slot];

        public void SetPosition(int slot, int position) => _rotors[slot].Position = position;

        public char EncodeLetter(char letter)
        {
            // Check for plugs
            letter = _plugboard.Substitute(letter);
            var letterAsInt = letter - 'A';

            // Pass through the 3 rotors
            letterAsInt = _rotors.Aggregate(letterAsInt, (current, t) => t.Substitute(current));

            // Hit the reflector
            letterAsInt = Reflector.Substitute(letterAsInt);
            
            // Pass back through the 3 rotors in reverse
            letterAsInt = _rotors.Reverse().Aggregate(letterAsInt, (current, t) => t.SubstituteBack(current));

            // Check for plugs and substitute as needed
            letter = (char) (letterAsInt + 'A');
            letter = _plugboard.Substitute(letter);
            
            // Rotate the first rotor
            _rotors[0].Rotate();

            // Return the encoded letter
            return letter;

        }
        
    }
}